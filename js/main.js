import { clearPlayingArea, drawLines } from './tablero.js'
import { gameState, stats, pointsToDraw, pointsToWinner } from './gameRules.js'
import { externalControlsCross } from './sceneThreeJSCross.js'
import { externalControlsRing } from "./sceneThreeJSRing.js";

let numeroJugadores = 0

let playerNames = {
    player1: {
        simbol: "x",
        name: ""
    },
    player2: {
        simbol: "o",
        name: ""
    }
}

//comienza X
var timer


var displayModalOptions = {
    "empate": "empate",
    "ganador": "gano"
};


(() => {
    addingEventListeners()
})()



function addingEventListeners() {


    document.querySelectorAll(".numero-jugadores").forEach(numJugadorEl => {
        numJugadorEl.addEventListener("click", (e) => {
            numeroJugadores = parseInt(e.target.innerHTML)
            goToInputNames()
        })
    });

    document.querySelector(".button.comenzar").addEventListener("click", () => {
        validarInputs()
    })

    document.getElementById("reiniciar-partida").addEventListener("click", () => {
        restartToInitialState()
    })

    document.getElementById("reiniciar-juego").addEventListener("click", () => {
        location.reload()
    })

}


function restartToInitialState() {

    for (let state in gameState) {
        gameState[state] = null
    }
    var element = document.querySelector(".timer > p")
    element.innerHTML = '00:00'
    clearTimeout(timer)
    counterUp()
    stats.currentGame += 1
    stats.currentMove = 0
    clearPlayingArea()
    drawLines()
    document.querySelector(".modal").style.display = "none"

    let puntosPlayer1 = stats.player1Wins * pointsToWinner + stats.draws * pointsToDraw
    let puntosPlayer2 = stats.player2Wins * pointsToWinner + stats.draws * pointsToDraw

    document.querySelector("#puntos-jugador-1").innerHTML = `Puntos: ${puntosPlayer1}`
    document.querySelector("#puntos-jugador-2").innerHTML = `Puntos: ${puntosPlayer2}`

    externalControlsCross.rotation = true
    externalControlsRing.rotation = false

}


function displayModal(option, ganador) {

    clearInterval(timer)

    let img = document.querySelector(".modal__header > img")
    let text = document.querySelector(".modal__header > h1")
    document.querySelector(".modal").style.display = "flex"

    switch (option) {
        case "empate":

            document.querySelector(".modal__header").style.flexDirection = "column"
            document.querySelector("#webgl3").style.display = "none"
            text.innerHTML = `Empate ganaron ${pointsToDraw} punto(s)`
            text.style.margin = "0px"
            img.src = "./src/empate_giphy.gif"
            img.style.width = "20%"
            img.style.position = "inherit"
            break
        case "gano":

            document.querySelector("#webgl3").style.display = "block"
            document.getElementById("audio-win").play()
            Object.values(playerNames).forEach((val) => {
                if (val.simbol == ganador) {
                    text.innerHTML = `Felicidades ganó ${val.name} ${pointsToWinner} puntos`
                }
            })

            if (numeroJugadores == 1 && ganador == "o") {
                text.innerHTML = `Ganó Computadora ${pointsToWinner} puntos`
                break
            }
            img.style.width = "10%"
            img.src = "./src/XZ5V.gif"
            img.style.position = "absolute"
            break;
    }
}


function goToInputNames(item) {

    document.querySelector(".menu").style.display = "none"
    switch (numeroJugadores) {
        case 2:
            let label = document.createElement("label")
            let input = document.createElement("input")
            let spanOut = document.createElement("span")
            let spanIn = document.createElement("span")
            label.className = "field field_v1"
            input.className = "field__input"
            input.id = "nombre-2"
            input.placeholder = "e.j. Karla"
            spanOut.className = "field__label-wrap"
            spanIn.className = "field__label"
            spanIn.innerHTML = "Jugador 2:"
            spanOut.appendChild(spanIn)

            label.appendChild(input)
            label.appendChild(spanOut)
            document.querySelector(".page").appendChild(label)

            numeroJugadores = 2
            break
    }

    document.querySelector(".initial-form__inputs").style.display = "block"

}

function validarInputs() {

    let nombre1 = document.querySelector("#nombre-1").value
    let nombre2 = document.querySelector("#nombre-2")
    let forma = document.querySelector(".initial-form")

    switch (numeroJugadores) {
        case 1:
            if (nombre1 != "") {
                forma.style.display = "none"
                setInitialValues(nombre1)
            } else {
                alertaNombreCorrecto()
            }

            break;
        case 2:
            if (nombre1 != "" && nombre2.value != "" && nombre1 !== nombre2.value) {

                forma.style.display = "none"
                setInitialValues(nombre1, nombre2.value)
            } else {
                alertaNombreCorrecto()
            }

            break;
    }

    function alertaNombreCorrecto() {
        alert("Favor de ingresar nombre de jugador correcto")
    }

}

function setInitialValues(...names) {

    var audio = new Audio("./src/audio/fondo.mp3");
    audio.volume = 0.2
    audio.play();

    playerNames.player1.name = names[0]
    names.length === 1 ? playerNames.player2.name = "Computadora" : playerNames.player2.name = names[1]

    document.querySelector("#nombre-jugador-1").innerHTML = playerNames.player1.name
    document.querySelector("#nombre-jugador-2").innerHTML = playerNames.player2.name

    counterUp()
}

function counterUp() {

    var second = 1;
    var minutes = 0
    timer = setInterval(function() {
        var extraZero = second < 10 ? '0' : '';
        var extraZeroMinutes = minutes < 10 ? '0' : ''

        var element = document.querySelector(".timer > p")
        element.innerHTML = `${extraZeroMinutes + minutes}:${extraZero+second}`

        if (second++ === 59) {
            minutes++
            second = 0;
        }
    }, 1000);
}





export { displayModal, displayModalOptions, numeroJugadores }