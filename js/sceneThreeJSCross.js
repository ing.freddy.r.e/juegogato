import * as THREE from './three.module.js';

let camera, scene, renderer;
let mesh, mesh2

const externalControlsCross = {
    rotation: true
}

init();
animate();

function init() {

    camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.z = 400;

    scene = new THREE.Scene();

    // const texture = new THREE.TextureLoader().load('textures/crate.gif');

    const geometry = new THREE.BoxBufferGeometry(10, 10, 300);
    const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    mesh = new THREE.Mesh(geometry, material);
    mesh.scale.set(1.5, 1.5, 1.5)
    mesh.rotation.set(1, 1, 0)

    const geometry2 = new THREE.BoxBufferGeometry(10, 10, 300);
    const material2 = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    mesh2 = new THREE.Mesh(geometry2, material2);
    mesh2.scale.set(1.5, 1.5, 1.5)
    mesh2.rotation.set(-1, 0.5, 0)

    scene.add(mesh);
    scene.add(mesh2)

    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    renderer.domElement.style.width = "100%"
    renderer.domElement.style.height = "100%"
    document.getElementById("webgl").appendChild(renderer.domElement)

    //window.addEventListener('resize', onWindowResize, false);

}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}

function animate() {

    requestAnimationFrame(animate);

    if (externalControlsCross.rotation) {
        mesh.rotation.x += 0.005;
        mesh.rotation.y += 0.01;

        mesh2.rotation.x += 0.005
        mesh2.rotation.y += 0.01

    }


    renderer.render(scene, camera);

}

export { externalControlsCross }