import * as THREE from './three.module.js';

let camera, scene, renderer;
let mesh
const externalControlsRing = {
    rotation: false
}
init();
animate();

function init() {

    camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.z = 400;

    scene = new THREE.Scene();

    // const texture = new THREE.TextureLoader().load('textures/crate.gif');

    const geometry = new THREE.RingGeometry(130, 110, 320);
    const material = new THREE.MeshBasicMaterial({ color: 0x04d9ff, side: THREE.DoubleSide });
    mesh = new THREE.Mesh(geometry, material);
    mesh.scale.set(1.5, 1.5, 1.5)
    mesh.rotation.set(1, 1, 0)


    scene.add(mesh);



    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    renderer.domElement.style.width = "100%"
    renderer.domElement.style.height = "100%"
    document.getElementById("webgl2").appendChild(renderer.domElement)

    // window.addEventListener('resize', onWindowResize, false);

}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}

function animate() {

    requestAnimationFrame(animate);

    if (externalControlsRing.rotation) {
        mesh.rotation.x += 0.005;
        mesh.rotation.y += 0.01;
    }


    renderer.render(scene, camera);

}

export { externalControlsRing }