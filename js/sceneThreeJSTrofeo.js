import * as THREE from './three.module.js';

import { OrbitControls } from './OrbitControls.js';
import { OBJLoader } from './OBJLoader.js';
import { RGBELoader } from './RGBELoader.js';

let container;
const params = {
    projection: 'normal',
    autoRotate: true,
    reflectivity: 1.0,
    background: false,
    exposure: 1.0,
    gemColor: 'Green'
};
let camera, scene, renderer;
let gemBackMaterial, gemFrontMaterial;
let hdrCubeRenderTarget;

const objects = [];

init();
animate();

function init() {


    camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.set(0.0, -10, 20 * 3.5);

    scene = new THREE.Scene();
    //scene.background = new THREE.Color(0x0000);

    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setClearColor(0xffffff, 0)
    gemBackMaterial = new THREE.MeshPhysicalMaterial({
        map: null,
        color: 0x0000ff,
        metalness: 1,
        roughness: 0,
        opacity: 0.5,
        side: THREE.BackSide,
        transparent: true,
        envMapIntensity: 5,
        premultipliedAlpha: true
            // TODO: Add custom blend mode that modulates background color by this materials color.
    });

    gemFrontMaterial = new THREE.MeshPhysicalMaterial({
        map: null,
        color: 0x0000ff,
        metalness: 0,
        roughness: 0,
        opacity: 0.25,
        side: THREE.FrontSide,
        transparent: true,
        envMapIntensity: 10,
        premultipliedAlpha: true
    });

    const manager = new THREE.LoadingManager();

    const loader = new OBJLoader(manager);
    loader.load('./src/emerald.obj', function(object) {

        object.traverse(function(child) {

            if (child instanceof THREE.Mesh) {

                child.material = gemBackMaterial;
                const second = child.clone();
                second.material = gemFrontMaterial;

                const parent = new THREE.Group();
                parent.add(second);
                parent.add(child);
                scene.add(parent);

                objects.push(parent);

            }

        });


    });

    new RGBELoader()
        .setDataType(THREE.UnsignedByteType)
        .setPath('./src/')
        .load('royal_esplanade_1k.hdr', function(hdrEquirect) {

            hdrCubeRenderTarget = pmremGenerator.fromEquirectangular(hdrEquirect);
            pmremGenerator.dispose();

            gemFrontMaterial.envMap = gemBackMaterial.envMap = hdrCubeRenderTarget.texture;
            gemFrontMaterial.needsUpdate = gemBackMaterial.needsUpdate = true;

            hdrEquirect.dispose();

        });

    const pmremGenerator = new THREE.PMREMGenerator(renderer);
    pmremGenerator.compileEquirectangularShader();

    // Lights

    scene.add(new THREE.AmbientLight(0x222222));

    const pointLight1 = new THREE.PointLight(0xffffff);
    pointLight1.position.set(150, 10, 0);
    pointLight1.castShadow = false;
    scene.add(pointLight1);

    const pointLight2 = new THREE.PointLight(0xffffff);
    pointLight2.position.set(-150, 0, 0);
    scene.add(pointLight2);

    const pointLight3 = new THREE.PointLight(0xffffff);
    pointLight3.position.set(0, -10, -150);
    scene.add(pointLight3);

    const pointLight4 = new THREE.PointLight(0xffffff);
    pointLight4.position.set(0, 0, 150);
    scene.add(pointLight4);

    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;


    renderer.domElement.style.width = "100%"
    renderer.domElement.style.height = "100%"
    document.getElementById("webgl3").appendChild(renderer.domElement);

    renderer.outputEncoding = THREE.sRGBEncoding;



    const controls = new OrbitControls(camera, renderer.domElement);
    controls.minDistance = 20;
    controls.maxDistance = 200;

    //window.addEventListener('resize', onWindowResize, false);

}

function onWindowResize() {

    const width = window.innerWidth;
    const height = window.innerHeight;

    camera.aspect = width / height;
    camera.updateProjectionMatrix();

    renderer.setSize(width, height);

}

//

function animate() {

    requestAnimationFrame(animate);

    render();

}

function render() {

    if (gemBackMaterial !== undefined && gemFrontMaterial !== undefined) {

        gemFrontMaterial.reflectivity = gemBackMaterial.reflectivity = params.reflectivity;

        let newColor = gemBackMaterial.color;
        switch (params.gemColor) {

            case 'Blue':
                newColor = new THREE.Color(0x000088);
                break;
            case 'Red':
                newColor = new THREE.Color(0x880000);
                break;
            case 'Green':
                newColor = new THREE.Color(0x008800);
                break;
            case 'White':
                newColor = new THREE.Color(0x888888);
                break;
            case 'Black':
                newColor = new THREE.Color(0x0f0f0f);
                break;

        }

        gemBackMaterial.color = gemFrontMaterial.color = newColor;

    }

    renderer.toneMappingExposure = params.exposure;

    camera.lookAt(scene.position);

    if (params.autoRotate) {

        for (let i = 0, l = objects.length; i < l; i++) {

            const object = objects[i];
            object.rotation.y += 0.005;

        }

    }

    renderer.render(scene, camera);

}

export { params }