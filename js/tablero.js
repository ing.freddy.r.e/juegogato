import { displayModal, displayModalOptions, numeroJugadores } from './main.js'
import { gameState, stats, calculateWinner, winnerPath } from './gameRules.js'
import { params } from "./sceneThreeJSTrofeo.js"
import { externalControlsCross } from "./sceneThreeJSCross.js"
import { externalControlsRing } from "./sceneThreeJSRing.js"

var actualPlayer = 0

var canvas = document.getElementById('canvas-juego');
var context = canvas.getContext('2d');
var canvasSize = 500;
var isPlaying = false
var allCoordsOrdered = []


//grid 3x3
var sectionSize = canvasSize / 3;
canvas.width = canvasSize;
canvas.height = canvasSize;

//anti-alising helper
context.translate(0.5, 0.5)


var lineStart = 5;
var lineLenght = canvasSize - lineStart;

var colorFichaO = "#04d9ff",
    colorFichaX = "#00ff00"



drawLines();
getAllCoords()

function drawLines() {
    context.lineCap = 'round';
    context.globalCompositeOperation = "lighter";
    context.beginPath();

    for (var y = 1; y <= 2; y++) {
        neonRect(lineStart, y * sectionSize, lineLenght, 1, 100, 0, 100);
    }
    for (var x = 1; x <= 2; x++) {
        neonRect(x * sectionSize, lineStart, 1, lineLenght, 100, 0, 100);
    }

}

function neonRect(x, y, w, h, r, g, b) {
    context.shadowColor = "rgb(" + r + "," + g + "," + b + ")";
    context.shadowBlur = 10;
    context.strokeStyle = "rgba(" + r + "," + g + "," + b + ",0.2)";
    context.lineWidth = 7.5;
    drawRectangle(x, y, w, h, 1.5);
    context.strokeStyle = "rgba(" + r + "," + g + "," + b + ",0.2)";
    context.lineWidth = 6;
    drawRectangle(x, y, w, h, 1.5);
    context.strokeStyle = "rgba(" + r + "," + g + "," + b + ",0.2)";
    context.lineWidth = 4.5;
    drawRectangle(x, y, w, h, 1.5);
    context.strokeStyle = "rgba(" + r + "," + g + "," + b + ",0.2)";
    context.lineWidth = 3;
    drawRectangle(x, y, w, h, 1.5);
    context.strokeStyle = '#fff';
    context.lineWidth = 1.5;
    drawRectangle(x, y, w, h, 1.5);
}

function drawRectangle(x, y, w, h, border) {
    context.beginPath();
    context.moveTo(x + border, y);
    context.lineTo(x + w - border, y);
    context.quadraticCurveTo(x + w - border, y, x + w, y + border);
    context.lineTo(x + w, y + h - border);
    context.quadraticCurveTo(x + w, y + h - border, x + w - border, y + h);
    context.lineTo(x + border, y + h);
    context.quadraticCurveTo(x + border, y + h, x, y + h - border);
    context.lineTo(x, y + border);
    context.quadraticCurveTo(x, y + border, x + border, y);
    context.closePath();
    context.stroke();
}



//Evento al click 

canvas.addEventListener('mouseup', function(event) {
    if (!isPlaying) {

        var canvasMousePosition = getCanvasMousePosition(event);
        addPlayingPiece(canvasMousePosition)

    }

})



function getCanvasMousePosition(event) {
    var rect = canvas.getBoundingClientRect();

    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    }
}


function getAllCoords() {
    var xCoordinate;
    var yCoordinate;
    for (var x = 0; x < 3; x++) {
        for (var y = 0; y < 3; y++) {
            xCoordinate = x * sectionSize;
            yCoordinate = y * sectionSize;
            allCoordsOrdered.push([xCoordinate, yCoordinate])
        }
    }

}


function addPlayingPiece(mouse) {

    var xCoordinate;
    var yCoordinate;

    for (var x = 0; x < 3; x++) {
        for (var y = 0; y < 3; y++) {
            xCoordinate = x * sectionSize;
            yCoordinate = y * sectionSize;

            if (
                mouse.x >= xCoordinate && mouse.x <= xCoordinate + sectionSize &&
                mouse.y >= yCoordinate && mouse.y <= yCoordinate + sectionSize
            ) {

                if (gameState[y * 3 + x] == null) {

                    actualPlayer === 1 ? actualPlayer = 2 : actualPlayer = 1
                    drawXorO(xCoordinate, yCoordinate, x, y)
                }

            }
        }
    }
}


const goComputer = () => {

    setTimeout(() => {

        actualPlayer = 2
        let statesAvailables = []

        gameState.forEach((state, index) => {
            if (state == null) {
                statesAvailables.push(index)
            }
        })

        let randIndex = Math.floor(Math.random() * statesAvailables.length) + 0
        let valueRand = statesAvailables[randIndex]
        drawXorO(allCoordsOrdered[valueRand][1], allCoordsOrdered[valueRand][0], valueRand, 0)

    }, 2000);

}


function drawXorO(xCoor, yCoor, x, y) {


    document.querySelector("#pop-audio").play()

    stats.currentMove += 1
    let option = ""

    if (actualPlayer == 1) {
        externalControlsCross.rotation = false
        externalControlsRing.rotation = true
        option = "x"
    } else {
        externalControlsCross.rotation = true
        externalControlsRing.rotation = false

        option = "o"

    }

    switch (option) {
        case "x":
            drawX(xCoor, yCoor);
            gameState[y * 3 + x] = "x"
            break;
        case "o":
            drawO(xCoor, yCoor);
            gameState[y * 3 + x] = "o"
            break;
    }

    let isDraw = (gameState.every((i) => {
        return i !== null
    }))

    if (isDraw) {

        externalControlsCross.rotation = true
        externalControlsRing.rotation = true
        stats.draws += 1
        actualPlayer = 0;
        displayModal(displayModalOptions.empate)

    }

    let winner = calculateWinner(gameState)
    if (winner) {

        let firstCoordPath = allCoordsOrdered[winnerPath[0]]
        let lastCoordPath = allCoordsOrdered[winnerPath[2]]
        drawWinLine(firstCoordPath, lastCoordPath)


        switch (winner) {
            case "x":
                params.gemColor = "Green"
                stats.player1Wins += 1
                externalControlsCross.rotation = true
                externalControlsRing.rotation = false
                break;
            case "o":
                params.gemColor = "Blue"
                stats.player2Wins += 1
                externalControlsCross.rotation = false
                externalControlsRing.rotation = true
                break;
        }
        actualPlayer = 0
        displayModal(displayModalOptions.ganador, winner)
    }

    isPlaying = false

    if (stats.currentMove % 2 && !winner && !isDraw && stats.currentMove < allCoordsOrdered.length && numeroJugadores == 1) {
        isPlaying = true
        goComputer()
    }


}


function drawWinLine(coordInt, coordFin) {

    context.shadowColor = "#FFFFFF";
    context.shadowBlur = 20;
    context.strokeStyle = "#FFFFFF";
    context.lineWidth = 10;
    context.beginPath();

    context.moveTo(coordInt[1] + sectionSize / 2, coordInt[0] + sectionSize / 2);
    context.lineTo(coordFin[1] + sectionSize / 2, coordFin[0] + sectionSize / 2);
    context.stroke();


}

function drawO(xCordinate, yCordinate) {

    var halfSectionSize = (0.5 * sectionSize);
    var centerX = xCordinate + halfSectionSize;
    var centerY = yCordinate + halfSectionSize;
    var radius = (sectionSize - 100) / 2;
    var startAngle = 0 * Math.PI;
    var endAngle = 2 * Math.PI;
    context.shadowColor = colorFichaO;
    context.shadowBlur = 20;
    context.strokeStyle = colorFichaO;
    context.lineWidth = 10;
    gsap.to(context, { lineWidth: 10, duration: 1 });
    context.beginPath();

    context.arc(centerX, centerY, radius, startAngle, endAngle);
    context.stroke();
}


function drawX(xCordinate, yCordinate) {

    var offset = 50;

    let coordObj = {
        xCoor: xCordinate,
        yCoor: yCordinate
    }


    context.shadowColor = colorFichaX;
    context.shadowBlur = 20;
    context.strokeStyle = colorFichaX;
    context.lineWidth = 10;
    context.beginPath();
    context.moveTo(coordObj.xCoor + offset, coordObj.yCoor + offset)

    context.lineTo(coordObj.xCoor + sectionSize - offset, coordObj.yCoor + sectionSize - offset)
    context.moveTo(coordObj.xCoor + offset, coordObj.yCoor + sectionSize - offset);
    context.lineTo(coordObj.xCoor + sectionSize - offset, coordObj.yCoor + offset);
    context.stroke();

}



function clearPlayingArea() {

    context.fillStyle = "black";
    context.clearRect(0, 0, canvas.width, canvas.height);

}


export { canvas, clearPlayingArea, drawLines }