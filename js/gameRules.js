const stats = {
    currentMove: 0,
    player1Wins: 0,
    player2Wins: 0,
    draws: 0
}

const winningConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];

const pointsToWinner = 3
const pointsToDraw = 1

const gameState = [null, null, null, null, null, null, null, null, null];

var winnerPath = []


function calculateWinner(gameState) {

    for (let i = 0; i < winningConditions.length; i++) {
        const [a, b, c] = winningConditions[i];
        if (gameState[a] && gameState[a] === gameState[b] && gameState[a] === gameState[c]) {
            winnerPath = [a, b, c]
            return gameState[a];
        }
    }
    return null;
}

export { gameState, stats, winningConditions, calculateWinner, winnerPath, pointsToDraw, pointsToWinner }